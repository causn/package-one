<?php
return [
    /*
      |--------------------------------------------------------------------------
      | Determine the paths
      |--------------------------------------------------------------------------
      |
     */
    "paths" => [
        "lang_folder" => base_path('lang')
    ],

    /*
      |--------------------------------------------------------------------------
      | The search options
      |--------------------------------------------------------------------------
      |
     */
    "search" => [

        /*
          |--------------------------------------------------------------------------
          | The folders the package uses to scan for translation keys
          |--------------------------------------------------------------------------
          |
         */
        "folders" => [
            base_path("app"),
            resource_path("views"),
        ],

        /*
          |--------------------------------------------------------------------------
          | Enter specific files
          |--------------------------------------------------------------------------
          |
         */
        "files" => [
        ]
    ],
    "source" => config('app.locale') ?? 'en', //custom "source" => "vi"
    "override" => true,
    "ui" => [
        'route' => true
    ],
//    "locales" => ['af','sq','am','ar','hy','as','ay','az','bm','eu','be','bn','bho','bs','bg','ca','ceb','zh-CN','zh-TW','co','hr','cs','da','dv',
//        'doi','nl','en','eo','et','ee','fil','fi','fr','fy','gl','ka','de','el','gn','gu','ht','ha','haw','he','hi','hmn','hu','is','ig',
//        'ilo','id','ga','it','ja','jv','kn','kk','km','rw','gom','ko','kri','ku','ckb','ky','lo','la','lv','ln','lt','lg','lb','mk','mai',
//        'mg','ms','ml','mt','mi','mr','mni-Mtei','lus','mn','my','ne','no','ny','or','om','ps','fa','pl','pt','pa','qu','ro','ru','sm','sa','gd','nso','sr','st','sn','sd','si','sk','sl','so','es','su','sw','sv','tl','tg',
//        'ta','tt','te','th','ti','ts','tr','tk','ak','uk','ur','ug','uz','vi','cy','xh','yi','yo','zu']
    "locales" => ['vi']
];
