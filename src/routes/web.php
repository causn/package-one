<?php

use Illuminate\Support\Facades\Route;
if(config('fast-labs-multiple-language.ui.route',true)){
    Route::get('fast-labs/multiple-language/translate', 'FastLabs\MultipleLanguage\Controllers\TranslateController@index')->name('fast-labs.multiple-language.index');
    Route::post('fast-labs/multiple-language/editor', 'FastLabs\MultipleLanguage\Controllers\TranslateController@editor')->name('fast-labs.multiple-language.editor');
    Route::get('fast-labs/multiple-language/list', 'FastLabs\MultipleLanguage\Controllers\TranslateController@list')->name('fast-labs.multiple-language.list');
}
