<?php

namespace FastLabs\MultipleLanguage\Library\GoogleTranslate\Exceptions;

use ErrorException;

class RateLimitException extends ErrorException
{
    //
}
