<?php

namespace FastLabs\MultipleLanguage\Library\GoogleTranslate\Exceptions;

use ErrorException;

class LargeTextException extends ErrorException
{
    //
}
