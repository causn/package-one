<?php

namespace FastLabs\MultipleLanguage\Library\GoogleTranslate\Exceptions;

class MultipleLanguageException extends \Exception
{
}
