<?php

namespace FastLabs\MultipleLanguage\Library\GoogleTranslate\Exceptions;

use ErrorException;

class TranslationRequestException extends ErrorException
{
    //
}
