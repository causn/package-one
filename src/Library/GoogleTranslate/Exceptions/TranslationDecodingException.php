<?php

namespace FastLabs\MultipleLanguage\Library\GoogleTranslate\Exceptions;

use UnexpectedValueException;

class TranslationDecodingException extends UnexpectedValueException
{
    //
}
