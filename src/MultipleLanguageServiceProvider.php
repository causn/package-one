<?php
namespace FastLabs\MultipleLanguage;
use FastLabs\MultipleLanguage\Commands\MultipleLanguageCommand;
use Illuminate\Support\ServiceProvider;

class MultipleLanguageServiceProvider extends ServiceProvider
{
    protected $commands = [
        MultipleLanguageCommand::class,
    ];
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/config.php' => config_path('fast-labs-multiple-language.php'),
            __DIR__.'/views' => resource_path('views/vendor/fast-labs/multiple-language'),
            __DIR__.'/resources/assets' => public_path('packages/fast-labs/multiple-language')
        ], 'fast-labs-multiple-language');

        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadViewsFrom(__DIR__.'/views', 'fast-labs-multiple-language');
    }
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/config/config.php',
            'fast-labs-multiple-language'
        );

        app()->config["filesystems.disks.fastLabsMultipleLanguage"] = [
            'driver' => 'local',
            'root' => config('fast-labs-multiple-language.paths.lang_folder'),
        ];

        $this->commands($this->commands);
    }
}
