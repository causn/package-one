<?php

namespace FastLabs\MultipleLanguage\Commands;

use Exception;
use FastLabs\MultipleLanguage\Library\GoogleTranslate\GoogleTranslate;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use function config;

class MultipleLanguageCommand extends Command
{
    /**
     * @var  string
     */
    protected $signature = 'fast-labs-multiple-language';

    /**
     * @var  string
     */
    protected $description = 'Automatically find, translate and save missing translation keys.';
    private GoogleTranslate $googleTranslate;
    public $files;
    public $keys;
    public $locales = [];
    public $translates;

    /**
     * @param GoogleTranslate $googleTranslate
     */
    public function __construct(GoogleTranslate $googleTranslate)
    {
        $this->googleTranslate = $googleTranslate;

        parent::__construct();
    }
    public function getFiles(): void
    {
        foreach (config('fast-labs-multiple-language.search.folders', []) as $folderPath){
            $files = File::allFiles($folderPath);
            foreach ($files as $file){
                $this->files[] = $file;
            }
        }

        foreach (config('fast-labs-multiple-language.search.files', []) as $filePath){
            $this->files[] = File::get($filePath);
        }
    }
    public function getLocales(): void
    {
        $this->locales = config('fast-labs-multiple-language.locales',[]);
        $this->locales[] = config('fast-labs-multiple-language.source','en');
        $this->locales = array_unique($this->locales);
        $this->locales = array_filter($this->locales);
    }
    private function formatContent($content): array|string
    {
        $functions = config('locale-finder.translation_methods', []);

        foreach ($functions as $function) {
            $pattern = '"';
            $content = str($content)
                ->replace("$function($pattern", "translationkeyFindedStart($pattern")
                ->replace("$pattern)", "{$pattern}translationkeyFindedEnd")
                ->value;

            $pattern = "'";
            $content = str($content)
                ->replace("$function($pattern", "translationkeyFindedStart($pattern")
                ->replace("$pattern)", "{$pattern}translationkeyFindedEnd")
                ->value;

            $pattern = "]";
            $content = str($content)
                ->replace("$function($pattern", "translationkeyFindedStart($pattern")
                ->replace("$pattern)", "{$pattern}translationkeyFindedEnd")
                ->value;
        }

        return str_replace("\n","",$content);
    }
    private function translateKey(string $locale, array|string $key): string
    {
        try {
            $this->googleTranslate->setSource(config('fast-labs-multiple-language.source','en'));
            $this->googleTranslate->setTarget($locale);
            $this->googleTranslate->preserveParameters();

            preg_match_all('/\s*:\s*([^ ]*)/m', $key, $matches, PREG_SET_ORDER, 0);
            $translated = $this->googleTranslate->translate($key);

        } catch (Exception $exception) {
            $this->error('Google translate issue with ' . $key . ': ' . $exception->getMessage());
            $translated = $key;
        }

        if (!$translated) {
            $translated = $key;
        }

        return $translated;
    }
    private function getKeysInFile($string): array
    {
        try {
            $re = '/(trans(?:_choice)?|Lang::(?:get|choice|trans(?:Choice)?)|@(?:lang|choice)|__)\(([\'"]([^\'"]+)[\'"])[)\]];?/';
            preg_match_all($re, $string, $matches, PREG_SET_ORDER, 0);
            $keys = [];

            foreach ($matches as $match) {
                if(strpos(end($match), ".")){
                    $arr = explode(".",end($match));
                    if(count($arr) > 1 && !empty(end($arr))){
                        $keys[][reset($arr)] = str_replace("_", " ", trim(end($arr)));
                    }else{
                        $keys[] = str_replace("_", " ", trim(reset($arr)));
                    }
                }else{
                    $keys[] = str_replace("_", " ", end($match));
                }
            }

            return $keys;
        }
        catch (Exception $exception){
            Log::error("Fast labs multiple language:".$exception->getMessage());
            return [];
        }
    }
    function getKeysInFiles(): void
    {
        foreach ($this->files as $file){
            $content = $this->formatContent($file->getContents());
            $keys = array_filter($this->getKeysInFile($content));

            foreach ($keys as $key => $value){
                if(is_array($value)){
                    foreach ($value as $k => $v){
                        $this->keys[$k][] = $v;
                    }
                }else{
                    $this->keys['all'][$value] = $value;
                }
            }
        }

        foreach ($this->keys as &$key){
            $key = array_filter($key);
            $key = array_unique($key);
        }
    }

    private function overrideKeyInFile($keys,$override,$newKeys)
    {
        if($override){
            return array_merge($keys,$newKeys);
        }else{
            return $keys + $newKeys;
        }
    }
    public function writeFileLanguage(): void
    {
        $root = config('fast-labs-multiple-language.paths.lang_folder', base_path('lang'));
        if (!File::isDirectory($root)) {
            File::makeDirectory($root, 0755, true);
        }

        foreach ($this->translates as $key => $value){
            foreach ($value as $k => $v) {
                if ($k === 'all') {
                    File::put($root . '/' . $key . '.json', json_encode($v, JSON_UNESCAPED_UNICODE));
                } else {
                    $pathDir = $root . '/' . $key;
                    if (!File::isDirectory($pathDir)) {
                        File::makeDirectory($pathDir, 0777, true);
                    }

                    $path = $pathDir. '/' . $k . '.php';
                    File::put($path, "<?php \n return " . var_export([], true) . ";");
                    $keys = @include $path;
                    File::put($path, "<?php \n return " . var_export($v, true) . ";");
                }
            }
            $this->info($key." language has been translated");
        }
    }
    public function handle(): int
    {
        $progressBar = $this->output->createProgressBar(4);
        $progressBar->setFormat(
            "In the process of %process%
                \n[%bar%]"
        );

        $this->getFiles();
        $progressBar->setMessage('Local keyword search','process');
        $progressBar->advance();
        $this->getKeysInFiles();

        $this->getLocales();
        $this->translates = [];

        $progressBar->setMessage('Translate local keywords','process');
        $progressBar->advance();
        foreach ($this->keys as $k => $v){
            foreach ($v as $k1 => $v1){
                if(is_array($this->locales)){
                    foreach ($this->locales as $local){
                        $this->translates[$local][$k][$v1] = $this->translateKey($local,$v1);
                    }
                }
            }
        }

        $progressBar->setMessage('Write data to file','process');
        $progressBar->advance();
        $this->writeFileLanguage();
        $progressBar->finish();

        return 1;
    }
}
