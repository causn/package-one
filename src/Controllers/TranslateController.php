<?php

namespace FastLabs\MultipleLanguage\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\File;
use Symfony\Component\Finder\Finder;

class TranslateController
{
    public function index()
    {
        $data = $this->getKeys();
        if(empty($data)){
            return view('fast-labs-multiple-language::nodata');
        }

        return view('fast-labs-multiple-language::index', compact('data'));
    }

    public function update()
    {
        return view('fast-labs-multiple-language::index');
    }

    public function list(Request $request)
    {
        if ($request->filled('local') || $request->filled('file_name')) {
            return null;
        }

        $data = $this->getKeys();

        $result = [];
        $result['data'] = null;

        if (!empty($data[$request->get('local')][$request->get('file_name')])) {
            $result['data'] = $data[$request->get('local')][$request->get('file_name')];
        }

        return json_encode($result);
    }

    public function editor(Request $request)
    {
        if (!empty($request->get('file_name')) && !empty($request->get('pk')) && !empty($request->get('local'))) {
            try {
                $word = $request->get('pk');
                $local = $request->get('local');
                $fileName = $request->get('file_name');
                $value = $request->get('value');

                $path = base_path('lang') . "/" . $local . "/" . $fileName . ".php";
                if (File::exists($path)) {
                    $keys = include $path;
                    if (isset($keys[$word])) {
                        $keys[$word] = $value;
                        File::replace($path, "<?php \n return " . var_export($keys, true) . ";");
                    }

                } else {
                    $path = base_path('lang') . "/" . $local . ".json";
                    $keys = json_decode(File::get($path), true);
                    if (isset($keys[$word])) {
                        $keys[$word] = $value;
                        File::replace($path, json_encode($keys, JSON_UNESCAPED_UNICODE));
                    }
                }

                return Response::json('success', 200);
            }catch (\Exception $exception){
                return Response::json('fail', 500);
            }
        }

        return Response::json('request is null', 428);
    }

    private function getKeys()
    {
        $finder = new Finder();
        $finder->files()->in(config('fast-labs-multiple-language.paths.lang_folder', null));

        $data = [];
        foreach ($finder as $file) {
            if ($file->getExtension() == 'php') {
                $folderName = $file->getRelativePath();
                $fileName = str_replace(".php", "", $file->getFilename());
                if (isset($data[$folderName][$fileName])) {
                    $data[$folderName][$fileName] = array_merge($data[$folderName][$fileName], include $file);
                } else {
                    $data[$folderName][$fileName] = include $file;
                }
            }

            if ($file->getExtension() == 'json') {
                $content = json_decode($file->getContents(), true);
                $fileName = str_replace(".json", "", $file->getFilename());
                if (isset($data[$fileName]['all'])) {
                    $data[$fileName]['all'] = array_merge($data[$fileName]['all'], $content);
                } else {
                    $data[$fileName]['all'] = $content;
                }
            }
        }
        return $data;
    }
}
