<!--example:https://jsfiddle.net/kztg62m5/-->
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bootstrap Accordion Menu for All Purpose</title>
    <link href="{{ asset('packages/fast-labs/multiple-language/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" id="bootstrap-css">
    <script src="{{ asset('packages/fast-labs/multiple-language/js/jquery.min.js') }}"></script>
    <script src="{{ asset('packages/fast-labs/multiple-language/bootstrap/js/bootstrap.min.js') }}"></script>
    <style>
        body{
            margin: 40px 0;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h1 class="text-center">No keys language in local</h1>
        </div>
    </div>
</div>
</body>
</html>
