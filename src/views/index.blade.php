<!--example:https://jsfiddle.net/kztg62m5/-->
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Bootstrap Accordion Menu for All Purpose</title>
        <link href="{{ asset('packages/fast-labs/multiple-language/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" id="bootstrap-css">
        <script src="{{ asset('packages/fast-labs/multiple-language/js/jquery.min.js') }}"></script>
        <script src="{{ asset('packages/fast-labs/multiple-language/bootstrap/js/bootstrap.min.js') }}"></script>
        <!--datatable-->
        <link rel="stylesheet" href="{{ asset('packages/fast-labs/multiple-language/datatables/css/datatables.min.css') }}">
        <script src="{{ asset('packages/fast-labs/multiple-language/datatables/js/datatables.min.js') }}"></script>
        <!--edit table-->
        <link rel="stylesheet" href="{{ asset('packages/fast-labs/multiple-language/x-editable/css/bootstrap-editable.css') }}">
        <script src="{{ asset('packages/fast-labs/multiple-language/x-editable/js/bootstrap-editable.min.js') }}"></script>
        <style>
            .well{
                display: none;
                width:100%;
            }
            body{
                margin: 40px 0;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                $(".file-translate").click(function () {
                    $(".file-translate").parent().parent().removeClass("active");
                    $(this).parent().parent().addClass('active');
                    let local = $(this).attr('data-local');
                    let key = $(this).attr('data-key');
                    $(".well").css('display', 'none');
                    $("#panel" + local + key).css('display', 'inline-block');
                });

                @php($index = 1)
                @foreach($data as $key => $item)
                @php($j = 1)
                @foreach($item as $k => $v)
                //datatable
                    <?php
                    $dataTable  = 'let table'.$key.$k.' = $("#table'.$key.$k.'").DataTable({';
                    //paging
                    $dataTable .= 'pagingType: "simple_numbers",';
                    //language
                    switch(config('app.locale')){
                        case 'en':
                            $locale = 'en-GB';
                            break;
                        case 'nl':
                            $locale = 'nl-NL';
                            break;
                        case 'vi':
                        default:
                            $locale = config('app.locale');
                            break;
                    }
                    $dataTable .= "language: {url: 'https://cdn.datatables.net/plug-ins/2.0.2/i18n/".$locale.".json'},";//https://cdn.datatables.net/plug-ins/2.0.2/i18n/
                    //bootstrap3-editable
                    $dataTable .= "drawCallback: function(settings){";
                    $dataTable .= "var api = this.api(); ";
                    $dataTable .= "$('.editable', api.table().body())";
                    $dataTable .= ".editable()";
                    $dataTable .= ".off('hidden')";
                    $dataTable .= ".on('hidden', function(e, reason) {";
                    $dataTable .= "if(reason === 'save') {";
                    $dataTable .= "$(this).closest('td').attr('data-order', $(this).text());";
                    $dataTable .= "table".$key.$k.".row($(this).closest('tr')).invalidate().draw(false);";
                    $dataTable .= "}";
                    $dataTable .= "});";
                    $dataTable .= "}";

                    $dataTable .= "});";
                    echo $dataTable;
                    ?>

                @php($j++)
                @endforeach
                @php($index++)
                @endforeach
            });
        </script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-md-3">
                    <div class="panel-group" id="accordion">
                        @php($index = 1)
                        @foreach($data as $key => $item)
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{Str::upper($key)}}">
                                            <span class="glyphicon glyphicon-flag"></span>{{Str::upper($key)}}
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse{{Str::upper($key)}}" class="panel-collapse collapse @if($index == 1) in @endif">
                                    <div class="panel-body">
                                        <table class="table">
                                            @php($j = 1)
                                            @foreach($item as $k => $v)
                                                <tr>
                                                    <td>
                                                        <span class="glyphicon glyphicon-file text-primary"></span>
                                                        <a href="javascript:void(0)"  data-local="{{$key}}" data-key="{{$k}}" class="file-translate">
                                                            {{Str::ucfirst($k)}}
                                                        </a>
                                                    </td>
                                                </tr>
                                            @php($j++)
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @php($index++)
                        @endforeach
                    </div>
                </div>
                <div class="col-sm-9 col-md-9">
                    @php($index = 1)
                    @foreach($data as $key => $item)
                        @php($j = 1)
                        @foreach($item as $k => $v)
                            <div class="well" @if($index == 1 && $j == 1) style="display: inline-block" @endif id="panel{{$key}}{{$k}}">
                                <h5>{{Str::upper($key)}} - {{Str::upper($k)}}</h5>
                                <table id="table{{$key}}{{$k}}" class="table table-striped table-bordered table-language " cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th style="width: 50%;">Key</th>
                                        <th style="width: 50%;">Translate</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php($g = 1)
                                    @foreach($v as $k1 => $v1)
                                        <tr data-key="{{$k1}}">
                                            <td style="width: 50%;">{{$k1}}</td>
                                            <td style="width: 50%;">
                                                <a href="javascript:void(0)"
                                                   id="translate_{{$index}}_{{$j}}_{{$g}}"
                                                   class="editable"
                                                   data-type="textarea"
                                                   data-params="{local:'<?php echo $key?>',file_name:'<?php echo $k;?>'}"
                                                   data-pk="{{$k1}}"
                                                   data-url="{{route('fast-labs.multiple-language.editor')}}"
                                                   data-mode="inline"
                                                   data-title="Translate:"
                                                   data-value="{{$v1}}">{{$v1}}</a>
                                            </td>
                                        </tr>
                                        @php($g++)
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @php($j++)
                        @endforeach
                    @php($index++)
                    @endforeach
                </div>
            </div>
        </div>
    </body>
</html>
